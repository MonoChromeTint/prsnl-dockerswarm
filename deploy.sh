#!/bin/sh
#sudo docker stack rm tset && sudo docker stack deploy -c ComposeFiles/tset.tml --prune=true tset
#sudo docker exec -it $(sudo docker ps -q -f name=tset_gitqlient.1) bash

## How to fix weird deployment issue
### Where docker will "deploy", but not schedule the containers
#
#sudo docker node ls
#
#sudo docker node demote <Node Name>
#
#sudo docker node promote <Node Name>
#
#sudo docker node ls

if [[ -z "$1" ]]; then
	echo "Error: No deployment named"
	exit
fi

docker stack rm $1

if [[ "$2" != "stop" ]]; then
	export $(cat .env) > /dev/null 2>&1
	docker stack deploy -c ComposeFiles/$1.yml --prune=true $1
fi
