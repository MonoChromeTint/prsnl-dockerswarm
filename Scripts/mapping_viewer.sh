#!/bin/bash



#for i in $(ls -1 ComposeFiles/); do
#	if [[ "$i" != "template.yml" ]]; then
#		if [[ "$i" != "tset.yml" ]]; then
#			echo "### $(echo $i | cut -d'.' -f1) ($(cat ComposeFiles/$i | grep 'NOTE' | cut -d':' -f2))"
#			NAMES=$(cat "ComposeFiles/$i" | grep -Ev '(    .*|version:|services:|net*)' | sed 's/^#.*//g' | sort | uniq | tr ' ' '_')
#			for xena in $(echo "$NAMES" | tr ',' ' '); do
#				nameName=$(echo $xena | tr '_' ' ' | cut -d':' -f1)
#				Description=$(echo $xena | tr '_' ' ' | cut -d'#' -f2)
#				echo "* $nameName - $Description" | tr -s ' '
#			done
#			echo -e "\n"
#		fi
#	fi
#done

ROOT_NAME="Cluster_Services_Swarm_"

LEVEL_ONE="       "
LEVEL_TWO="         "



echo "/* BEGIN DOCKER SWARM MAPPING */
  subgraph Cluster_Services {
    bgcolor="white"
    label="Docker"

    subgraph Cluster_Services_Swarm {
      label=\"Swarm: Deployments\""
for i in $(ls -1 ComposeFiles/); do
	if [[ "$i" != "template.yml" ]]; then
        if [[ "$i" != "tset.yml" ]]; then
                tmpName="$(echo $i | cut -d'.' -f1)"
                locPRE="$(echo $ROOT_NAME$tmpName | tr '_' '\n' | tr -d '[a-z]' | cut -c 1 | tr -d '\n')"
#                locPOST="_$(echo $ROOT_NAME$tmpName | tr '_' '\n' | tr -d '[A-Z]' | cut -c 1-3 | tr -d '\n')"
                locPOST="$(echo $ROOT_NAME$tmpName | tr '_' '\n' | grep -Ev '([A-Z])' | cut -c 1-3)"

		aArray=""

		echo "$LEVEL_ONE subgraph $ROOT_NAME$tmpName {"
                echo "$LEVEL_TWO label=\"$i\""
                echo "$LEVEL_TWO color=green;"
#                echo "$locPRE$locPOST"
                echo ""

		NAMES=$(cat "ComposeFiles/$i" | grep -Ev '(    .*|version:|services:|net*)' | sed 's/^#.*//g' | sort | uniq | tr ' ' '_')
		NumNum=0
		for xena in $(echo "$NAMES" | tr ',' ' '); do
			nameName=$(echo $xena | cut -d':' -f1 | tr -s '_')
			cutName=$(echo $nameName | cut -c 1-4)
			cleanName=$(echo $nameName | cut -d'_' -f2)
			Description=$(echo $xena | tr '_' ' ' | cut -d'#' -f3)
			echo "$LEVEL_TWO $locPRE$locPOST$cutName [shape=box,color=black,label=\"$cleanName:\n$Description\"];"

			if [[ "$aArray" == "" ]]; then
				aArray="$locPRE$locPOST$cutName"
			else
				aArray="$aArray -> $locPRE$locPOST$cutName"
			fi
			NumNum=$((NumNum+1))
		done

                echo ""
		if [[ $NumNum == "1" ]]; then
			echo "$LEVEL_TWO $aArray;"
		else
			echo "$LEVEL_TWO $aArray [style=invis];"
		fi
                echo "$LEVEL_ONE }"

	        echo -e "\n"
	fi
	fi

done

echo "    }
/*END DOCKER SWARM MAPPING*/"
