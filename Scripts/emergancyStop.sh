#!/bin/bash

for i in $(docker stack ls | tr -s ' ' | cut -d' ' -f1); do
	if [[ "$i" != "NAME" ]]; then
		echo "Shutting down: '$i'"
		docker stack rm '$i'"
	fi
done
