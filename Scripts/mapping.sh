#!/bin/bash

GRP=""
ICOs=""

XXX=1
YYY=0


for i in $(ls -1 ComposeFiles/); do
	if [[ "$i" != "template.yml" ]]; then
		if [[ "$i" != "tset.yml" ]]; then
			echo "$i"
#			cat "ComposeFiles/$i" | grep -Ev '(    .*|#|version:|services:|net*)' | sort | uniq
			NAMES=$(cat "ComposeFiles/$i" | grep -Ev '(    .*|#|version:|services:|net*)' | sort | uniq | tr ':' ' ' | tr -d ' ' | tr '\n' ',' | sed 's/.$//g' | sed 's/^.//g')
			for xena in $(echo "$NAMES" | tr ',' ' '); do
				if [[ $(($YYY % 3)) == 0 ]]; then
#					XXX=$(())
					XXX=$(($XXX+1))
				fi
				ICOs="$ICOs"$(echo -e "\n  "$xena": {<<: *defaults, x: "$XXX", y: "$YYY", iconFill: green}")

				YYY=$((YYY+1))
			done
			GRP="$GRP"$(echo -e "\n  - { <<: *group, name: "$i", members: ["$NAMES"] }")
			echo -e "\n============="
		fi
	fi

done



GRPE="group: &group
  - { color: \"white\", stroke: \"lightgrey\", fill: \"rgba(0, 0, 0, 0.3)\" }"



echo -e "icons:$ICOs"
echo -e "$GRPE\ngroups:$GRP"
