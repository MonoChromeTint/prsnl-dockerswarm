#!/bin/bash

for i in $(docker stack ls | tr -s ' ' | cut -d' ' -f1); do
	if [[ "$i" != "NAME" ]]; then
		echo "Deployment: '$i'"
		docker stack ps "$i"

		echo
	fi
done
